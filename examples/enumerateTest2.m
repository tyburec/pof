% computation
yfac = 0.75;
nAngles = 100;
angles = linspace(0,pi/2,nAngles);
nsdp = cell(nAngles,1);
fcon = cell(nAngles,1);
oc   = cell(nAngles,1);
mom  = cell(nAngles,1);
ocObj = nan(nAngles,1);
fminObj = nan(nAngles,1);
nsdpObj = nan(nAngles,1);
trussObj = nan(nAngles,1);
frameLB = nan(nAngles,1);
for i=3:-1:1
    rlbObj{i} = nan(nAngles,1);
    rubObj{i} = nan(nAngles,1);
end
for i=1:nAngles-1
    fprintf('enumerateTest2(): Problem %d\n',i);
    run('multipleGlobalOptima');
    frameObject.nodes(:,2) = frameObject.nodes(:,2)*yfac;
    frameObject.nodes(:,1) = frameObject.nodes(:,1)/yfac;
    frameObject.loadFx{1} = cos(angles(i));
    frameObject.loadFy{1} = sin(angles(i));
    sumOfSquaresObject.stopBasedOnBounds = false;
    problem = framesBuildProblem(frameObject);
%     nsdp{i} = framesNonlinearSDP(problem, nonlinearSDPObject);
%     nsdpObj(i) = nsdp{i}.objective;
%     fcon{i} = framesFmincon(problem, fminconObject);
%     fminObj(i) = fcon{i}.objective;
%     oc{i} = framesOptimalityCriteria(problem, optimalityCriteriaObject);
%     ocObj(i) = oc{i}.objective;
%     trussObj(i) = framesTrussSDPUpperBound(problem);
%     frameLB(i) = framesSDPLowerBound(problem);
    mom{i} = sumOfSquaresObject;
    for relaxation=1:sumOfSquaresObject.maximumRelaxationNumber
        mom{i} = framesSumOfSquaresHierarchy(problem, mom{i}, relaxation);
        rlbObj{relaxation}(i) = mom{i}.objectiveLowerBound(relaxation);
        rubObj{relaxation}(i) = mom{i}.objectiveUpperBound(relaxation);
        if mom{i}.isOptimal
            break;
        end
    end
end

% recover angles with multiple global optima
out = nan(1,nAngles);
sol = nan(nAngles,3);
for i=1:nAngles, if isfield(mom{i}, 'solution'), out(i) = size(mom{i}.solution,2); else, out(i) = nan; end; end
for i=1:nAngles, if isfield(mom{i}, 'solution'), sol(i,:) = mom{i}.solution(:,1); end; end
rchSol1 = sol(2:end,1)-sol(1:end-1,1);
cand = find(abs(rchSol1)>0.2);
optAngle = nan(1,numel(cand));
optObj = nan(1,numel(cand));
for i=1:numel(cand)
    if size(mom{cand(i)}.solution,2)>1
        optAngle(i) = angles(cand(i));
        optObj(i) = mom{cand(i)}.objective;
    elseif size(mom{cand(1)+1}.solution,2)>1
        optAngle(i) = angles(cand(i)+1);
        optObj(i) = mom{cand(i)+1}.objective;
    else
        lAngle = angles(cand(i));
        lSol = sol(cand(i),:);
        uAngle = angles(cand(i)+1);
        uSol = sol(cand(i)+1,:);
        
        % bisection
        iter = 1;
        while true
            fprintf('Langle=%f, uangle=%f\n', lAngle, uAngle);
            if iter>1000
                break;
            end
            
            if abs(uAngle-lAngle)<1e-12
                break;
            end
            
            mAngle = mean([lAngle, uAngle]);
            run('test2');
            frameObject.loadFx{1} = cos(mAngle);
            frameObject.loadFy{1} = sin(mAngle);
            sumOfSquaresObject.stopBasedOnBounds = false;
            problem = framesBuildProblem(frameObject);
            optmom = sumOfSquaresObject;
            for relaxation=1:sumOfSquaresObject.maximumRelaxationNumber
                optmom = framesSumOfSquaresHierarchy(problem, optmom, relaxation);
                if optmom.isOptimal
                    break;
                end
            end
            
            if size(optmom.solution,2)>1
                optAngle(i) = mAngle;
                optObj(i) = optmom.objective;
                break;
            else
                mSol = optmom.solution';
                if norm(uSol-mSol)>norm(mSol-lSol)
                    lSol = mSol;
                    lAngle = mAngle;
                else
                    uSol = mSol;
                    uAngle = mAngle;
                end
            end
            
            iter = iter+1;
        end
    end
end

% plotting
hold on;
h1lb = plot(angles, rlbObj{1}, '--k','LineWidth',2.75);
h1ub = plot(angles, rubObj{1}, '-.k','LineWidth',2.50);
h2lb = plot(angles, rlbObj{2}, '--c','LineWidth',2.25);
h2ub = plot(angles, rubObj{2}, '-.c','LineWidth',2.00);
h3lb = plot(angles, rlbObj{3}, '--y','LineWidth',1.75);
h3ub = plot(angles, rubObj{3}, '-.y','LineWidth',1.50);

hNsdp = plot(angles, nsdpObj, '-.r','LineWidth',3.50);
hOc = plot(angles, ocObj, '-.g','LineWidth',3.25);
hFmin = plot(angles, fminObj, '-.b','LineWidth',3.00);
hT = plot(angles, trussObj, '-.m', 'LineWidth', 1.25);
hL = plot(angles, frameLB, '--m', 'LineWidth', 1.00);
mg = scatter(optAngle, optObj, 80, 'h'); 
xlabel('$\theta$','Interpreter','latex');
ylabel('$\omega^\mathrm{T} \mathbf{c}$','Interpreter','latex');
xlim([0, pi/2]);
xticks([0 pi/8 pi/4 3*pi/8 pi/2]);
xticklabels({'0','\pi/8','\pi/4','3\pi/8','\pi/2'});
legend([hNsdp, hOc, hFmin, h1lb, h1ub, h2lb, h2ub, h3lb, h3ub, hT, hL, mg], 'NSDP', 'OC', 'fmincon','r1lb','r1ub','r2lb','r2ub','r3lb','r3ub','truss UB','frame LB','multiple optima', 'Location','northwest','Interpreter','latex');
set(gca, 'YScale', 'log');
hold off;

print(['./output/test2_enumeration',num2str(yfac),'.png'],'-dpng','-r300');
print(['./output/test2_enumeration',num2str(yfac),'.pdf'],'-dpdf','-r300');
saveas(gcf,['./output/test2_enumeration',num2str(yfac),'.fig']);