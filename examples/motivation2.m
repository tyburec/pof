frameObject.crossSection                    = 'rectangle';
frameObject.getPlottingParam                = @(x) deal(sqrt(12*x)/12, sqrt(12*x), zeros(size(x)), zeros(size(x)));
frameObject.factor2                         = 1.0;
frameObject.factor3                         = 0.0;
frameObject.E                               = 1.0;
frameObject.density                         = 0.0;
frameObject.volumeBound                     = 1.0;
frameObject.nodes                           = [0.0 0.0; 1.0 0.5; 0.0 1.0];
frameObject.numberOfNodes                   = size(frameObject.nodes,1);
frameObject.elements                        = [1 2; 2 3];
frameObject.numberOfElements                = size(frameObject.elements,1);
frameObject.numberOfLoadCases               = 1;
frameObject.complianceWeights               = 1;
frameObject.loadFx{1}                       = -1.6;
frameObject.loadIdFx{1}                     = 2;
frameObject.loadFy{1}                       = 1.0;
frameObject.loadIdFy{1}                     = 2;
frameObject.loadMz{1}                       = [];
frameObject.loadIdMz{1}                     = [];
frameObject.idUx{1}                         = [1 3];
frameObject.idUy{1}                         = [1 3];
frameObject.idFiz{1}                        = [1 3];
frameObject.elementType                     = 'euler-bernoulli';