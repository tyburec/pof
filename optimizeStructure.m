function [nonlinearSDPObject, fminconObject, optimalityCriteriaObject, mmaObject, momentSumOfSquaresObject] = optimizeStructure(fname, plotOutput)
    try 
        run(fname);
    catch
        addpath(genpath('.'));
    end

    if ~exist('plotOutput','var')
        plotOutput = true;
    end

    % load problem definition
    run(fname);

    % initialize problem data (FEM)
    if exist('frameObject','var')
        problem = framesBuildProblem(frameObject);
    elseif exist('shellObject','var')
        problem = shellsBuildProblem(shellObject);
    end

    % non-linear SDP
    if ~exist('nonlinearSDPObject', 'var')
        nonlinearSDPObject = defaultNonlinearSDPObject(problem.numberOfElements, problem.numberOfLoadCases);
    end
    nonlinearSDPObject = framesNonlinearSDP(problem, nonlinearSDPObject);
    if plotOutput
        plotStructure(problem, nonlinearSDPObject.areas);
        saveas(gcf,['output/',fname, '_nsdp.png']); saveas(gcf,['output/',fname, '_nsdp.pdf']);
    end

    % fmincon
    if ~exist('fminconObject', 'var')
        fminconObject = defaultFminconObject(problem.numberOfElements, problem.numberOfLoadCases);
    end
    fminconObject = framesFmincon(problem, fminconObject);
    if plotOutput
        plotStructure(problem, fminconObject.areas);
        saveas(gcf,['output/',fname, '_fmincon.png']); saveas(gcf,['output/',fname, '_fmincon.pdf']);
    end

    % optimality criteria
    if ~exist('optimalityCriteriaObject', 'var')
        optimalityCriteriaObject = defaultOptimalityCriteriaObject(problem.numberOfElements, problem.numberOfLoadCases);
    end
    optimalityCriteriaObject = framesOptimalityCriteria(problem, optimalityCriteriaObject);
    if plotOutput
        plotStructure(problem, optimalityCriteriaObject.areas);
        saveas(gcf,['output/',fname, '_oc.png']); saveas(gcf,['output/',fname, '_oc.pdf']);
    end

    % method of moving assymptotes
    if ~exist('mmaObject','var')
        mmaObject = defaultMMAObject(problem.numberOfElements, problem.numberOfLoadCases);
    end
    mmaObject = framesMMA(problem, mmaObject);
    if plotOutput
        plotStructure(problem, mmaObject.areas);
        saveas(gcf,['output/',fname, '_mma.png']); saveas(gcf,['output/',fname, '_mma.pdf']);
    end

    % polynomial optimization
    if ~exist('momentSumOfSquaresObject', 'var')
        momentSumOfSquaresObject = defaultMomentSumOfSquaresObject(problem.numberOfElements, problem.numberOfLoadCases);
    end
    minRelaxation = 1 + (problem.factor3>0.0)*1;
    for relaxation=minRelaxation:momentSumOfSquaresObject.maximumRelaxationNumber
        momentSumOfSquaresObject = framesSumOfSquaresHierarchy(problem, momentSumOfSquaresObject, relaxation);
        if plotOutput
            plotStructure(problem, momentSumOfSquaresObject.areas{relaxation});
            saveas(gcf,['output/',fname, '_po',num2str(relaxation),'.png']); saveas(gcf,['output/',fname, '_po',num2str(relaxation),'.pdf']);
        end
        if isfield(momentSumOfSquaresObject,'solution')
            for sol=1:size(momentSumOfSquaresObject.solution,2)
                if plotOutput
                    plotStructure(problem, momentSumOfSquaresObject.solution(1:problem.numberOfElements,sol));
                    saveas(gcf,['output/',fname, '_poRANK',num2str(sol),'.png']); saveas(gcf,['output/',fname, '_poRANK',num2str(sol),'.pdf']);
                end
            end
        end
        if momentSumOfSquaresObject.isOptimal
            break;
        end
    end
end