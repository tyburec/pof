function [frameUBound, ares] = framesTrussSDPUpperBound(problem)

    opts = sdpsettings('cachesolvers', true);
    opts.verbose = false;

    % optimization variables
    a = sdpvar(problem.numberOfElements,1);
    c = sdpvar(problem.numberOfLoadCases,1);

    % constraints
    LMIconstraints = cell(size(c));
    for lc=1:problem.numberOfLoadCases
        f = problem.fConst{lc}(problem.dofs(:,lc)) + problem.fSelfweight(problem.dofs(:,lc),:)*a;
        K = problem.kElementsMembraine{1}(problem.dofs(:,lc),problem.dofs(:,lc))*a(1);
        for e=2:problem.numberOfElements
            K = K + problem.kElementsMembraine{e}(problem.dofs(:,lc),problem.dofs(:,lc))*a(e);
        end
        LMIconstraints{lc} = [c(lc) -f'; -f K]>=0;
    end

    % solution
    diagnostics = solvesdp([LMIconstraints{:}; a>=0; problem.elementLengths'*a<=problem.volumeBound], problem.complianceWeights*c, opts);

    if diagnostics.problem~=0
        fprintf('framesTrussSDPUpperBound(): not feasible');
    end

    trussUBound = problem.complianceWeights*value(c);
    frameUBound = computeCompliance(problem, value(a));
    ares = value(a);
    
    if isinf(frameUBound)
        trussUBound = Inf;
    end
    
    fprintf('framesTrussSDPUpperBound(): ubtruss=%f\n', trussUBound);
    fprintf('framesTrussSDPUpperBound(): ubtrussasframe=%f\n', frameUBound);
    
    if (frameUBound>trussUBound+eps)
        error('framesTrussSDPUpperBound(): frame posseses higher compliance than truss. This can not happen!');
    end
end