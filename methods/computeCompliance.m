function out = computeCompliance(problem, areas)
    out = 0;
    omega = problem.complianceWeights;
    for lc=1:problem.numberOfLoadCases
        Kreal = zeros(nnz(problem.dofs(:,lc)));
        freal = problem.fConst{lc}(problem.dofs(:,lc)) + problem.fSelfweight(problem.dofs(:,lc),:)*areas;
        for e=1:problem.numberOfElements
            Kreal = Kreal + problem.kElementsMembraine{e}(problem.dofs(:,lc),problem.dofs(:,lc))*areas(e) + ...
                        (problem.factor2>0.0)*problem.kElementsBending{e}(problem.dofs(:,lc),problem.dofs(:,lc))*areas(e)^2 + ...
                        (problem.factor3>0.0)*problem.kElementsBending{e}(problem.dofs(:,lc),problem.dofs(:,lc))*areas(e)^3;
        end
        ureal = pinv(Kreal)*freal;
        % check whether f(a) \in Image(K(a))
        if norm(Kreal*ureal-freal)>min(abs(freal(freal~=0)))*1e-6
            out = Inf;
            warning(['computeCompliance() produced Inf compliance. Error ', num2str(norm(Kreal*ureal-freal))]);
            break;
        else
            out = out + omega(lc)*freal'*ureal;
        end
    end
end