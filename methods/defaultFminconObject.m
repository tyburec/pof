function fminconObject = defaultFminconObject(nel,nlc)
    fminconObject.areas                         = nan(nel,1);
    fminconObject.objective                     = nan;
    fminconObject.verboseSolver                 = false;
    fminconObject.time                          = nan;
end