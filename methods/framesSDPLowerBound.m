function lBound = framesSDPLowerBound(problem)

    opts = sdpsettings('cachesolvers', true);
    opts.verbose = false;

    % optimization variables
    a = sdpvar(problem.numberOfElements,1);
    a2 = sdpvar(problem.numberOfElements,1);
    c = sdpvar(problem.numberOfLoadCases,1);

    % constraints
    LMIconstraints = cell(size(c));
    for lc=1:problem.numberOfLoadCases
        f = problem.fConst{lc}(problem.dofs(:,lc)) + problem.fSelfweight(problem.dofs(:,lc),:)*a;
        K = problem.kElementsMembraine{1}(problem.dofs(:,lc),problem.dofs(:,lc))*a(1) + problem.kElementsBending{1}(problem.dofs(:,lc),problem.dofs(:,lc))*a2(1);
        for e=2:problem.numberOfElements
            K = K + problem.kElementsMembraine{e}(problem.dofs(:,lc),problem.dofs(:,lc))*a(e) + problem.kElementsBending{e}(problem.dofs(:,lc),problem.dofs(:,lc))*a2(e);
        end
        LMIconstraints{lc} = [c(lc) -f'; -f K]>=0;
    end
    
    % maximum areas
    if problem.factor2>0.0
        amax2 = ((problem.volumeBound*ones(problem.numberOfElements,1))./problem.elementLengths).^2*problem.factor2;
        relConstr = [a2<=amax2; a2>=a.^2];
    else
        error('3-degree polynomials currently not supported');
    end

    % solution
    diagnostics = solvesdp([LMIconstraints{:}; a>=0; relConstr; problem.elementLengths'*a<=problem.volumeBound], problem.complianceWeights*c, opts);

    if diagnostics.problem~=0
        fprintf('framesSDPLowerBound(): not feasible');
        lBound = nan;
    else
        lBound = problem.complianceWeights*value(c);
    end
    
    fprintf('framesSDPLowerBound(): %f\n', lBound);

end