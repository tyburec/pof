function nonlinearSDPObject = defaultNonlinearSDPObject(nel, nlc)
    nonlinearSDPObject.areas                    = nan(nel,1);
    nonlinearSDPObject.objective                = nan;
    nonlinearSDPObject.verboseSolver            = false;
    nonlinearSDPObject.time                     = nan;
end