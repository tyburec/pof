function optimalityCriteriaObject = defaultOptimalityCriteriaObject(nel,nlc)
    optimalityCriteriaObject.minimumArea        = 1e-8;
    optimalityCriteriaObject.maximumIterations  = 1e3;
    optimalityCriteriaObject.minimumChange      = 1e-6;
    optimalityCriteriaObject.move               = 0.2;
    optimalityCriteriaObject.moveIncreaseFactor = 2.0;
    optimalityCriteriaObject.moveIncreaseBound  = 0.5;
    optimalityCriteriaObject.moveDecreaseFactor = 0.75;
    optimalityCriteriaObject.moveDecreaseBound  = 0.1;
    optimalityCriteriaObject.areas              = nan(nel);
    optimalityCriteriaObject.objective          = nan;
    optimalityCriteriaObject.verboseSolver      = false;
end