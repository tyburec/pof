function problem = framesBuildProblem(frameObject)
    problem = frameObject;

    % checks
    if size(frameObject.nodes,1)~=frameObject.numberOfNodes
        error('Number of nodes is not consistent with the nodes matrix.');
    end
    if size(frameObject.elements,1)~=frameObject.numberOfElements
        error('Number of elements is not consistent with the elements matrix.');
    end
    if frameObject.numberOfLoadCases~=size(frameObject.complianceWeights,2)
        error('Number of load cases is not consistent with the lenght of the complieanceWeights vector.');
    end
    if size(frameObject.complianceWeights,1)~=1
        error('ComplianceWeights vector must have one row only.');
    end
    if (frameObject.factor2>0 && frameObject.factor3>0)
        error('Only one of factor2 and factor3 can be nonzero.');
    else
        factor = frameObject.factor2 + frameObject.factor3;
    end
    if frameObject.factor2<0.0
        error('Factor2 must be non-negative.');
    end
    if frameObject.factor3<0.0
        error('Factor3 must be non-negative.');
    end
    elementType = 'euler-bernoulli';
    if isfield(frameObject, 'elementType')
        switch frameObject.elementType
            case 'euler-bernoulli'
                elementType = 'euler-bernoulli';

            case 'timoshenko'
                elementType = 'timoshenko';
                if ~isfield(frameObject,'nu')
                    error('framesBuildProblem(): Timoshenko beam element must be supplied with nu field.');
                end
                if ~isfield(frameObject,'k')
                    error('framesBuildProblem(): Timoshenko beam element must be supplied with k field.');
                end

            otherwise
                error('framesBuildProblem(): Unknown element type.');
        end
    end
    
    nodes = frameObject.nodes;
    x = nodes(:,1);
    y = nodes(:,2);
    elements = frameObject.elements;
    dx = x(elements(:,2))-x(elements(:,1));
    dy = y(elements(:,2))-y(elements(:,1));
    l = sqrt(dx.^2 + dy.^2);

    % boundary conditions
    fConst = cell(1,frameObject.numberOfLoadCases);
    dofs = true(frameObject.numberOfNodes*3,frameObject.numberOfLoadCases);
    for lc=1:frameObject.numberOfLoadCases
        fConst{lc} = sparse([frameObject.loadIdFx{lc}*3-2 frameObject.loadIdFy{lc}*3-1 frameObject.loadIdMz{lc}*3], ....
                            ones(numel(frameObject.loadFx{lc})+numel(frameObject.loadFy{lc})+numel(frameObject.loadMz{lc}),1), ...
                            [frameObject.loadFx{lc}       frameObject.loadFy{lc}       frameObject.loadMz{lc}], ...
                            frameObject.numberOfNodes*3, 1);
        dofs([frameObject.idUx{lc}*3-2 frameObject.idUy{lc}*3-1 frameObject.idFiz{lc}*3],lc) = false;
    end

    % loop over elements
    kElementsBending   = cell(frameObject.numberOfElements,1);
    kElementsMembraine = cell(frameObject.numberOfElements,1);
    fSelfweight        = cell(frameObject.numberOfElements,1);
    for e=1:frameObject.numberOfElements
        n1 = elements(e,1);
        n2 = elements(e,2);
        codeNums = [3*n1-2:3*n1 3*n2-2:3*n2];
        
        % stiffnesses
        switch elementType
            case 'euler-bernoulli'
                [kElementsMembraine_T, kElementsBending_T] = KeFrame2dFixed(x(n1), y(n1), x(n2), y(n2), frameObject.E, 1.0, factor);

            case 'timoshenko'
                [kElementsMembraine_T, kElementsBending_T] = KeFrame2dFixedTim(x(n1), y(n1), x(n2), y(n2), frameObject.E, frameObject.nu, 1.0, factor, frameObject.k);

            otherwise
                error('framesBuildProblem(): Unknown element type.');
        end
        kElementsMembraine{e} = spalloc(frameObject.numberOfNodes*3,frameObject.numberOfNodes*3,nnz(kElementsMembraine_T));
        kElementsBending{e} = spalloc(frameObject.numberOfNodes*3,frameObject.numberOfNodes*3,nnz(kElementsBending_T));
        kElementsMembraine{e}(codeNums,codeNums) = kElementsMembraine_T;
        kElementsBending{e}(codeNums,codeNums) = kElementsBending_T;

        % selfweight
        fSelfweight{e} = sparse([3*n1-1 3*n2-1],[1 1],-0.5*frameObject.density*l(e)*[1 1], frameObject.numberOfNodes*3, 1);
    end    

    problem.elementLengths = l;
    problem.dofs = dofs;
    problem.fConst = fConst;
    problem.fSelfweight = [fSelfweight{:}];
    problem.kElementsBending = kElementsBending;
    problem.kElementsMembraine = kElementsMembraine;
end