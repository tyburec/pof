function problem = shellsBuildProblem(shellObject)
    problem = shellObject;

    % checks
    if size(shellObject.nodes,1)~=shellObject.numberOfNodes
        error('Number of nodes is not consistent with the nodes matrix.');
    end
    if size(shellObject.elements,1)~=shellObject.numberOfElements
        error('Number of elements is not consistent with the elements matrix.');
    end
    if size(shellObject.elements,2)~=4
        error('Each shell must be supplied with 4 nodes.');
    end
    if shellObject.numberOfLoadCases~=size(shellObject.complianceWeights,2)
        error('Number of load cases is not consistent with the lenght of the complieanceWeights vector.');
    end
    if size(shellObject.complianceWeights,1)~=1
        error('ComplianceWeights vector must have one row only.');
    end
    if (shellObject.factor2~=0)
        error('Second-order polynomial of thickness for shells is not supported.');
    end
    if shellObject.factor3<0.0
        error('Factor3 must be non-negative.');
    end
    
    nodes = shellObject.nodes;
    x = nodes(:,1);
    y = nodes(:,2);
    elements = shellObject.elements;

    % compute shell elements areas
    areas = zeros(shellObject.numberOfElements,1);
    for e=1:shellObject.numberOfElements
        enodes = nodes(elements(e,:),:);
        aa = enodes(2,:)-enodes(1,:);
        bb = enodes(3,:)-enodes(2,:);
        cc = enodes(4,:)-enodes(3,:);
        p = bb+cc;
        q = aa+bb;
        areas(e) = 0.5*norm((p(1)*q(2) - p(2)*q(1)));
    end
    l = areas;  % to stay consistent with frames notation

    % boundary conditions
    fConst = cell(1,shellObject.numberOfLoadCases);
    dofs = true(shellObject.numberOfNodes*5,shellObject.numberOfLoadCases);
    for lc=1:shellObject.numberOfLoadCases
        fConst{lc} = sparse([shellObject.loadIdFx{lc}*5-4 shellObject.loadIdFy{lc}*5-3 shellObject.loadIdFz{lc}*5-2 ...
                             shellObject.loadIdMx{lc}*5-1 shellObject.loadIdMy{lc}*5], ....
                            ones(numel(shellObject.loadFx{lc})+numel(shellObject.loadFy{lc})+numel(shellObject.loadFz{lc})+...
                            numel(shellObject.loadMx{lc})+numel(shellObject.loadMy{lc}),1), ...
                            [shellObject.loadFx{lc} shellObject.loadFy{lc} shellObject.loadFz{lc} shellObject.loadMx{lc} shellObject.loadMy{lc}], ...
                            shellObject.numberOfNodes*5, 1);
        dofs([shellObject.idUx{lc}*5-4 shellObject.idUy{lc}*5-3 shellObject.idUz{lc}*5-2 shellObject.idFix{lc}*5-1 shellObject.idFiy{lc}*5],lc) = false;
    end

    % loop over elements
    kElementsBending   = cell(shellObject.numberOfElements,1);
    kElementsMembraine = cell(shellObject.numberOfElements,1);
    fSelfweight        = cell(shellObject.numberOfElements,1);
    for e=1:shellObject.numberOfElements
        n = elements(e,:);
        codeNums = repmat(1:5,1,4) + kron(n-1,[5 5 5 5 5]);
        
        % stiffnesses
        [kElementsMembraine_T, kElementsBending_T] = KeShell2dFixed(shellObject.nu, shellObject.E, shellObject.k, x(n), y(n));        
        kElementsMembraine{e} = spalloc(shellObject.numberOfNodes*5,shellObject.numberOfNodes*5,nnz(kElementsMembraine_T));
        kElementsBending{e} = spalloc(shellObject.numberOfNodes*5,shellObject.numberOfNodes*5,nnz(kElementsBending_T));
        kElementsMembraine{e}(codeNums,codeNums) = kElementsMembraine_T;
        kElementsBending{e}(codeNums,codeNums) = kElementsBending_T;

        % selfweight
        x1 = x(1); x2 = x(2); x3 = x(3); x4 = x(4);
        y1 = y(1); y2 = y(2); y3 = y(3); y4 = y(4);
        fSelfweight{e} = sparse(3:5:18, ones(4,1), -problem.density * ...
                          [(x1*y2)/6 - (x2*y1)/6 - (x1*y4)/6 + (x2*y3)/12 - (x3*y2)/12 + (x4*y1)/6 + (x2*y4)/12 - (x4*y2)/12 + (x3*y4)/12 - (x4*y3)/12; ...
                          (x1*y2)/6 - (x2*y1)/6 - (x1*y3)/12 + (x3*y1)/12 - (x1*y4)/12 + (x2*y3)/6 - (x3*y2)/6 + (x4*y1)/12 + (x3*y4)/12 - (x4*y3)/12; ...
                          (x1*y2)/12 - (x2*y1)/12 - (x1*y4)/12 + (x2*y3)/6 - (x3*y2)/6 + (x4*y1)/12 - (x2*y4)/12 + (x4*y2)/12 + (x3*y4)/6 - (x4*y3)/6; ...
                          (x1*y2)/12 - (x2*y1)/12 + (x1*y3)/12 - (x3*y1)/12 - (x1*y4)/6 + (x2*y3)/12 - (x3*y2)/12 + (x4*y1)/6 + (x3*y4)/6 - (x4*y3)/6], ...
                          shellObject.numberOfNodes*5, 1);
    end

    problem.elementLengths = l;
    problem.dofs = dofs;
    problem.fConst = fConst;
    problem.fSelfweight = [fSelfweight{:}];
    problem.kElementsBending = kElementsBending;
    problem.kElementsMembraine = kElementsMembraine;
end