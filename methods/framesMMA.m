function mmaSetting = framesMMA(problem, mmaSetting)

    % options
    maxIter     = mmaSetting.maximumIterations;
    minChange   = mmaSetting.minimumChange;
    amin        = mmaSetting.minimumArea;

    % initialization
    iter        = 1;
    change      = 1;
    l           = problem.elementLengths;
    V           = problem.volumeBound;
    a           = V/sum(l)*ones(size(l));
    u           = cell(problem.numberOfLoadCases,1);
    nelem       = problem.numberOfElements;
    cPath       = nan(maxIter,1);

    % mma
    numCons = 1;
    numVars = numel(a);
    amin = ones(numVars,1)*amin;
    amax = repmat(V,numVars,1)./l;
    aold1 = a; aold2 = a;
    low = amin;
    upp = amax;

    if mmaSetting.verboseSolver
        fprintf('Starting MMA optimizaton...\n');
    end

    tic
    while change>minChange
        if iter>maxIter
            break;
        end

        % solution using FEM
        cPath(iter) = 0;
        for lc=1:problem.numberOfLoadCases
            f = problem.fConst{lc}(problem.dofs(:,lc)) + problem.fSelfweight(problem.dofs(:,lc),:)*a;
            K = problem.kElementsMembraine{1}(problem.dofs(:,lc),problem.dofs(:,lc))*a(1) + ...
                (problem.factor2>0.0)*problem.kElementsBending{1}(problem.dofs(:,lc),problem.dofs(:,lc))*a(1)^2 + ...
                (problem.factor3>0.0)*problem.kElementsBending{1}(problem.dofs(:,lc),problem.dofs(:,lc))*a(1)^3;
            for e=2:problem.numberOfElements
                K = K + problem.kElementsMembraine{e}(problem.dofs(:,lc),problem.dofs(:,lc))*a(e) + ...
                    (problem.factor2>0.0)*problem.kElementsBending{e}(problem.dofs(:,lc),problem.dofs(:,lc))*a(e)^2 + ...
                    (problem.factor3>0.0)*problem.kElementsBending{e}(problem.dofs(:,lc),problem.dofs(:,lc))*a(e)^3;
            end
            u{lc} = K\f;
            cPath(iter) = cPath(iter)+problem.complianceWeights(lc)*f'*u{lc};
        end

        % sensitivities
        dc = zeros(nelem,1);
        for e=1:nelem
            for lc=1:problem.numberOfLoadCases
                K0elem = problem.kElementsMembraine{e}(problem.dofs(:,lc),problem.dofs(:,lc)) + ...
                    (problem.factor2>0.0)*2*a(e)*problem.kElementsBending{e}(problem.dofs(:,lc),problem.dofs(:,lc)) + ...
                    (problem.factor3>0.0)*3*a(e)^2*problem.kElementsBending{e}(problem.dofs(:,lc),problem.dofs(:,lc));
                dc(e) = dc(e) - u{lc}'*K0elem*u{lc} + (problem.complianceWeights(lc)+1)*u{lc}'*problem.fSelfweight(problem.dofs(:,lc),e);
            end
        end
        dv = l;

        f0val = cPath(iter);
        df0dx = dc;
        df0dx2 = zeros(numVars,1);  % neglected
        fconval = l'*a/V - 1;
        dfcondx = dv/V;
        dfcondx2 = zeros(1,numVars);
        a0 = 1;
        am = 0;
        ci = 10000;
        d = 0;
        
        [anew,~,~,~,~,~,~,~,~,low,upp,~,~] = ...
            mmasub(numCons, numVars, iter, a, amin, amax, aold1, aold2, ...
            f0val, df0dx, df0dx2, fconval, dfcondx, dfcondx2, low, upp, a0, am, ci, d);
                
        change = max(abs(anew(:)-a(:)));
        aold2 = aold1;
        aold1 = a;
        a = anew;
        if mmaSetting.verboseSolver
            fprintf('Iter %5i: Obj.:%11.4f Vol.:%7.3f ch.:%3.3e\n',iter,cPath(iter),l'*aold1,change);
        end
        
        iter = iter+1;
        
    end
    mmaSetting.time = toc;
    mmaSetting.areas = a;
    mmaSetting.objective = cPath(iter-1);

    fprintf('framesMMA(): %f\n', computeCompliance(problem,a));
        
end