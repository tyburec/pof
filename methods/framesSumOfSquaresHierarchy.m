function [sumOfSquaresObject, momentProblem] = framesSumOfSquaresHierarchy(problem, sumOfSquaresObject, relaxation)

    solver = 'yalmip';  % also gloptipoly

    omega = problem.complianceWeights';
    opts = sdpsettings('cachesolvers', true);
    opts.verbose = sumOfSquaresObject.verboseSolver;

    % optimization variables
    if strcmp(solver,'yalmip')
        asc = sdpvar(problem.numberOfElements,1);
        csc = sdpvar(problem.numberOfLoadCases,1);
    elseif strcmp(solver,'gloptipoly')
        mpol('asc', problem.numberOfElements, 1);
        mpol('csc', problem.numberOfLoadCases, 1);
    else
        error('Unknown solver');
    end

    % bounds for the cross-sections
    alb = sumOfSquaresObject.areasLowerBounds;
    aub = sumOfSquaresObject.areasUpperBounds;
    for e=1:problem.numberOfElements
        aub(e) = min(aub(e), problem.volumeBound/problem.elementLengths(e));
        alb(e) = max(alb(e), 0);
    end
    a = (aub+alb)/2 + (aub-alb)/2.*asc;

    % compliances based on uniform design V=factor*l'*a
    clb = sumOfSquaresObject.compliancesLowerBounds;
    cub = sumOfSquaresObject.compliancesUpperBounds;
    aUniform = ones(problem.numberOfElements,1)*problem.volumeBound/sum(problem.elementLengths);
    cUniform = computeCompliance(problem, aUniform);
    if ~isnan(sumOfSquaresObject.customComplianceUpperBound)
        cUniform = sumOfSquaresObject.customComplianceUpperBound;
    end
    for lc=1:problem.numberOfLoadCases
        cub(lc) = min(cub(lc), cUniform);
        clb(lc) = max(clb(lc), 0);
    end
    if all(isnan(sumOfSquaresObject.objectiveLowerBound))
        fprintf('framesSumOfSquaresHierarchy(): ubuni=%f\n', cUniform);
    end

    % lower-bound compliance from previous relaxation
    if (relaxation>1) && (sumOfSquaresObject.dynamicLowerBounds) && (problem.numberOfLoadCases==1)
        cLowerRelaxation = sumOfSquaresObject.objectiveLowerBound(relaxation-1);
        clb = max(clb, cLowerRelaxation);
    end

    % Lagrangian dual -- compute lower-bound compliance
    if sumOfSquaresObject.useLagrangianLowerBound && (problem.numberOfLoadCases==1)
        cLagrangian = framesLagrangianDualSDP(problem);
        clb(lc) = max(clb, cLagrangian);
    end

    c = (cub+clb)./(2*omega) + (cub-clb)./(2*omega).*csc;

    % constraints
    PMIconstraints = cell(size(c));
    f = cell(problem.numberOfLoadCases,1);
    K = cell(problem.numberOfLoadCases,1);
    for lc=1:problem.numberOfLoadCases
        if problem.factor2>0
            Kmult = a(1)^2;
        elseif problem.factor3>0
            Kmult = a(1)^3;
        else
            Kmult = 0;
        end
        f{lc} = problem.fConst{lc}(problem.dofs(:,lc)) + problem.fSelfweight(problem.dofs(:,lc),:)*a;
        K{lc} = problem.kElementsMembraine{1}(problem.dofs(:,lc),problem.dofs(:,lc))*a(1) + ...
                    problem.kElementsBending{1}(problem.dofs(:,lc),problem.dofs(:,lc))*Kmult;
        for e=2:problem.numberOfElements
            if problem.factor2>0
                Kmult = a(e)^2;
            elseif problem.factor3>0
                Kmult = a(e)^3;
            else
                Kmult = 0;
            end
            K{lc} = K{lc} + problem.kElementsMembraine{e}(problem.dofs(:,lc),problem.dofs(:,lc))*a(e) + ...
                        problem.kElementsBending{e}(problem.dofs(:,lc),problem.dofs(:,lc))*Kmult;
        end
        PMIconstraints{lc} = [c(lc) -f{lc}'; -f{lc} K{lc}]>=0;
    end
    objBounds = [];
    if lc>1
        objBounds = omega'*csc<=1;
    end

    % bound constraints
    if (problem.factor3>0.0)
        boundConstraints = [asc.^4<=1; csc.^4<=1];
    else
        boundConstraints = [asc.^2<=1; csc.^2<=1];
    end

    % solution
    if strcmp(solver,'yalmip')
        global rankdrop ranktol;
        rankdrop = 1e-3;
        ranktol = 1e-8;
        [diagnostics, solution, momentdata] = solvemoment([PMIconstraints{:}; boundConstraints; 2-problem.numberOfElements-sum(asc)>=0; objBounds], omega'*c, opts, relaxation);
    elseif strcmp(solver, 'gloptipoly')
        error('Gloptipoly does not support PMIs yet!');
        msdp([PMIconstraints{:}; boundConstraints; 2-problem.numberOfElements-sum(asc)>=0; objBounds], min(omega'*c))
    else
        error('Unknown solver');
    end
    
    % moment ranks (copied function from Yalmip)
    moments = momentdata.moment;
    ranks = zeros(1, length(moments));
    for i = 1:length(moments)
        [~,S,~] = svd(moments{i});
        s = diag(S);
        decay = s(2:end)./(eps+s(1:end-1));
        r = min(find(decay<rankdrop));
        if isempty(r)
            ranks(i) = rank(moments{i},ranktol);
        else
            ranks(i) = min(r,rank(moments{i},ranktol));
        end
    end
    
    enforced_solution = [];
    if ranks(end)~=ranks(end-1)
        options = opts; %options.moment.extractrank = nnz(r>1e-1);
        rankdrop = 1; ranktol = 1e-6;
        enforced_solution = extractsolution(momentdata,options);
    end
     
    sumOfSquaresObject.areas{relaxation} = relaxvalue(a);
    sumOfSquaresObject.objectiveLowerBound(relaxation) = omega'*relaxvalue(c);
    sumOfSquaresObject.objectiveUpperBound(relaxation) = computeCompliance(problem, sumOfSquaresObject.areas{relaxation});
    sumOfSquaresObject.time(relaxation) = diagnostics.solvertime;

    % return the moment problem as the second output argument if requested
    if nargout>1
        [constraintsRelaxed, objectiveRelaxed, momentMatrices] = momentmodel([PMIconstraints{:}; boundConstraints; 2-problem.numberOfElements-sum(asc)>=0; objBounds], omega'*c, relaxation);
        momentProblem.objective = omega'*c;     % to keep track with scaling
        momentProblem.areas = a;                % to keep track with scaling
        momentProblem.constraintsRelaxed = constraintsRelaxed;
        momentProblem.objectiveRelaxed = objectiveRelaxed;
        momentProblem.momentMatrices = momentMatrices;
        momentProblem.relaxation = relaxation;
        momentProblem.objectiveUniform = cUniform;
    end

    fprintf('framesSumOfSquaresHierarchy(): r=%d, lb=%f, ub=%f, gap=%f\n', relaxation, sumOfSquaresObject.objectiveLowerBound(relaxation), sumOfSquaresObject.objectiveUpperBound(relaxation), sumOfSquaresObject.objectiveUpperBound(relaxation)-sumOfSquaresObject.objectiveLowerBound(relaxation));
    stoppingCondition = '';
    endL = '';

    % check global optimality
    boundDiff = sumOfSquaresObject.objectiveUpperBound(relaxation) - sumOfSquaresObject.objectiveLowerBound(relaxation);
    if sumOfSquaresObject.stopBasedOnBounds
        if (boundDiff/sumOfSquaresObject.objectiveLowerBound(relaxation))<=sumOfSquaresObject.optimalGapTolerance
            sumOfSquaresObject.isOptimal = true;
            sumOfSquaresObject.objective = sumOfSquaresObject.objectiveUpperBound(relaxation);
            stoppingCondition = ['bound-equality (',num2str(boundDiff,'%10.2e'),')'];
            %return
        end
    end
    for ns=1:numel(enforced_solution)
        assign(momentdata.x, enforced_solution{ns});
        sumOfSquaresObject.enforced_solution(:,ns) = [value(a); computeCompliance(problem, value(a))];
        fprintf('framesSumOfSquaresHierarchy(): enforced extraction of atom %d: cub = %f, Vfrac = %f, amin = %f.\n', ns, sumOfSquaresObject.enforced_solution(end,ns), problem.elementLengths'*value(a)/problem.volumeBound, min(value(a)));
    end
    if sumOfSquaresObject.stopBasedOnRank
        if (~isempty(solution))
            sumOfSquaresObject.isOptimal = true;
            sumOfSquaresObject.objective = omega'*value(c);
            sumOfSquaresObject.solution = zeros(1+numel(a),numel(solution));
            for ns=1:numel(solution)
                assign(momentdata.x, solution{ns});
                sumOfSquaresObject.solution(:,ns) = [value(a); omega'*value(c)];
            end
            if isempty(stoppingCondition)
                stoppingCondition = ['rank (', num2str(ranks(end)), ')'];
            else
                stoppingCondition = [stoppingCondition, ' and rank (', num2str(ranks(end)), ')'];
                endL = 's';
            end
            %return
        end
    end

    if ~isempty(stoppingCondition)
        fprintf('framesSumOfSquaresHierarchy(): converged based on ');
        fprintf('%s', stoppingCondition);
        fprintf(' condition%s.\n', endL);
    elseif sumOfSquaresObject.maximumRelaxationNumber==relaxation
        fprintf('framesSumOfSquaresHierarchy(): not converged. Higher relaxation number is needed.\n');
    end

end