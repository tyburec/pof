function ocSetting = framesOptimalityCriteria(problem, ocSetting)

    % options
    maxIter     = ocSetting.maximumIterations;
    minChange   = ocSetting.minimumChange;
    amin        = ocSetting.minimumArea;
    move        = ocSetting.move;

    % initialization
    iter        = 1;
    change      = 1;
    l           = problem.elementLengths;
    V           = problem.volumeBound;
    a           = V/sum(l)*ones(size(l));
    u           = cell(problem.numberOfLoadCases,1);
    nelem       = problem.numberOfElements;
    cPath       = nan(maxIter,1);

    if ocSetting.verboseSolver
        fprintf('Starting Optimality Criteria optimizaton...\n');
    end

    tic
    while change>minChange
        if iter>maxIter
            break;
        end

        % solution using FEM
        cPath(iter) = 0;
        for lc=1:problem.numberOfLoadCases
            f = problem.fConst{lc}(problem.dofs(:,lc)) + problem.fSelfweight(problem.dofs(:,lc),:)*a;
            K = problem.kElementsMembraine{1}(problem.dofs(:,lc),problem.dofs(:,lc))*a(1) + ...
                (problem.factor2>0.0)*problem.kElementsBending{1}(problem.dofs(:,lc),problem.dofs(:,lc))*a(1)^2 + ...
                (problem.factor3>0.0)*problem.kElementsBending{1}(problem.dofs(:,lc),problem.dofs(:,lc))*a(1)^3;
            for e=2:problem.numberOfElements
                K = K + problem.kElementsMembraine{e}(problem.dofs(:,lc),problem.dofs(:,lc))*a(e) + ...
                    (problem.factor2>0.0)*problem.kElementsBending{e}(problem.dofs(:,lc),problem.dofs(:,lc))*a(e)^2 + ...
                    (problem.factor3>0.0)*problem.kElementsBending{e}(problem.dofs(:,lc),problem.dofs(:,lc))*a(e)^3;
            end
            u{lc} = K\f;
            cPath(iter) = cPath(iter)+problem.complianceWeights(lc)*f'*u{lc};
        end

        % sensitivities
        dc = zeros(nelem,1);
        for e=1:nelem
            for lc=1:problem.numberOfLoadCases
                K0elem = problem.kElementsMembraine{e}(problem.dofs(:,lc),problem.dofs(:,lc)) + ...
                    (problem.factor2>0.0)*2*a(e)*problem.kElementsBending{e}(problem.dofs(:,lc),problem.dofs(:,lc)) + ...
                    (problem.factor3>0.0)*3*a(e)^2*problem.kElementsBending{e}(problem.dofs(:,lc),problem.dofs(:,lc));
                dc(e) = dc(e) - u{lc}'*K0elem*u{lc} + (problem.complianceWeights(lc)+1)*u{lc}'*problem.fSelfweight(problem.dofs(:,lc),e);
            end
        end
        dv = l;

        % OPTIMALITY CRITERIA UPDATE (based on bisection)
        l1 = 0; 
        l2 = 1e9;
        while (l2-l1)/(l1+l2) > 2e-16
            lmid = 0.5*(l2+l1); % lmid is the current value of mu
            anew = max(amin, ...
                       max(a*(1-move),...
                           min(a*(1+move), ...
                               a.*nthroot(-dc./dv/lmid,3))...
                           )...
                       );
            if l'*anew > V
                l1 = lmid;
            else
                l2 = lmid;
            end
        end
        
        change = max(abs(anew(:)-a(:)));
        avgchange = mean(abs(anew(:)-a(:)));
        
        if avgchange/move >= ocSetting.moveIncreaseBound
            move = move*ocSetting.moveIncreaseFactor;
        elseif avgchange/move <= ocSetting.moveDecreaseBound
            move = move*ocSetting.moveDecreaseFactor;
        end
        
        a = anew;
        if ocSetting.verboseSolver
            fprintf('Iter %5i: Obj.:%11.4f Vol.:%7.3f ch.:%7.3f\n',iter,cPath(iter),l'*a,change);
        end
        
        iter = iter+1;
        
    end
    ocSetting.time = toc;
    ocSetting.areas = a;
    ocSetting.objective = cPath(iter-1);

    fprintf('framesOptimalityCriteria(): %f\n', cPath(iter-1));
        
end