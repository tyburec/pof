function fminconObject = framesFmincon(problem, fminconObject)

    opts = sdpsettings('cachesolvers', true);
    opts.verbose = fminconObject.verboseSolver;

    % optimization variables
    a = sdpvar(problem.numberOfElements,1);
    u = cell(problem.numberOfLoadCases,1);
    for lc=1:problem.numberOfLoadCases
        u{lc} = sdpvar(nnz(problem.dofs(:,lc)),1);
    end
    
    % constraints
    equilibriumEquations = cell(size(u));
    for lc=1:problem.numberOfLoadCases
        f = problem.fConst{lc}(problem.dofs(:,lc)) + problem.fSelfweight(problem.dofs(:,lc),:)*a;
        K = problem.kElementsMembraine{1}(problem.dofs(:,lc),problem.dofs(:,lc))*a(1) + ...
            (problem.factor2>0.0)*problem.kElementsBending{1}(problem.dofs(:,lc),problem.dofs(:,lc))*a(1)^2 + ...
            (problem.factor3>0.0)*problem.kElementsBending{1}(problem.dofs(:,lc),problem.dofs(:,lc))*a(1)^3;
        for e=2:problem.numberOfElements
            K = K + problem.kElementsMembraine{e}(problem.dofs(:,lc),problem.dofs(:,lc))*a(e) + ...
                (problem.factor2>0.0)*problem.kElementsBending{e}(problem.dofs(:,lc),problem.dofs(:,lc))*a(e)^2 + ...
                (problem.factor3>0.0)*problem.kElementsBending{e}(problem.dofs(:,lc),problem.dofs(:,lc))*a(e)^3;
        end
        equilibriumEquations{lc} = K*u{lc} - f == 0;
        if lc==1
            objective = problem.complianceWeights(lc)*f'*u{lc};
        else
            objective = objective + problem.complianceWeights(lc)*f'*u{lc};
        end
    end
    
    % solution
    diagnostics = solvesdp([equilibriumEquations{:}, a>=0; problem.elementLengths'*a<=problem.volumeBound], objective, opts);
    
    fminconObject.time = diagnostics.solvertime;
    if diagnostics.problem==0
        fminconObject.areas = value(a);
        fminconObject.objective = computeCompliance(problem, value(a));
        if abs(fminconObject.objective-value(objective))/max([fminconObject.objective value(objective)])>=1e-6
            warning(['Computed compliance ', num2str(fminconObject.objective), ' does not match the optimized value of ', num2str(value(objective)),'!']);
        end
    else 
        fminconObject.objective = Inf;
    end

    fprintf('framesFmincon(): %f\n', fminconObject.objective);

 end