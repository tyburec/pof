function MMAObject = defaultMMAObject(nel,nlc)
    MMAObject.minimumArea        = 1e-8;
    MMAObject.maximumIterations  = 1e3;
    MMAObject.minimumChange      = 1e-6;
    MMAObject.areas              = nan(nel);
    MMAObject.objective          = nan;
    MMAObject.verboseSolver      = false;
end