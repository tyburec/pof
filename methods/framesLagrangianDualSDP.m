function lbound = framesLagrangianDualSDP(problem)

    opts = sdpsettings('cachesolvers', true, 'solver', 'sedumi');

    if (problem.factor3>0.0)
        error('3-degree polynomial currently not supported');
    end

    % optimization variables
    lambda = sdpvar(1);
    U = cell(problem.numberOfLoadCases,1);
    for lc=1:problem.numberOfLoadCases
        U{lc} = sdpvar(nnz(problem.dofs(:,lc))+1);
    end

    % loadcase-wise constraints
    loadCasesWeightsConstraints = cell(problem.numberOfLoadCases,1);
    PositiveSemidefiniteUU   = cell(problem.numberOfLoadCases,1);
    for lc=1:problem.numberOfLoadCases
        PositiveSemidefiniteUU{lc} = U{lc}>=0;
        loadCasesWeightsConstraints{lc} = U{lc}(end,end) == problem.complianceWeights(lc);
        Q0 = [zeros(nnz(problem.dofs(:,lc))) problem.fConst{lc}(problem.dofs(:,lc)); ...
              problem.fConst{lc}(problem.dofs(:,lc))' 0];
        if (lc==1)
            Q0U = sum(Q0(:).*U{lc}(:));
        else
            Q0U = Q0U + sum(Q0(:).*U{lc}(:)); 
        end
    end

    % element-wise constraints
    energyConstraints = cell(problem.numberOfElements,1);
    zeroConstraints  = cell(problem.numberOfElements,1);
    for e=1:problem.numberOfElements
        for lc=1:problem.numberOfLoadCases
            Q2 = [problem.kElementsBending{1}(problem.dofs(:,lc),problem.dofs(:,lc)) zeros(nnz(problem.dofs(:,lc)),1); ...
                  zeros(1,1+nnz(problem.dofs(:,lc)))];
            Q1 = [problem.kElementsMembraine{e}(problem.dofs(:,lc),problem.dofs(:,lc)), problem.fSelfweight(problem.dofs(:,lc),e);...
                  problem.fSelfweight(problem.dofs(:,lc),e)' 0];
            if lc==1
                zeroConstraints{e} = sum(Q2(:).*U{lc}(:));
                energyConstraints{e} = sum(Q1(:).*U{lc}(:));
            else
                zeroConstraints{e} = zeroConstraints{e} + sum(Q2(:).*U{lc}(:));
                energyConstraints{e} = energyConstraints{e} + sum(Q1(:).*U{lc}(:));
            end
        end
        energyConstraints{e} = lambda*problem.elementLengths(e) - energyConstraints{e} >= 0;
        zeroConstraints{e} = zeroConstraints{e} == 0;
    end

    % solution
    objective = Q0U + lambda*problem.volumeBound;
    solvesdp([PositiveSemidefiniteUU{:}; lambda>=0; loadCasesWeightsConstraints{:}; energyConstraints{:}; zeroConstraints{:}], objective, opts);

    lbound = value(objective);

    fprintf('framesLagrangianDualSDP(): %f\n', value(objective));

end