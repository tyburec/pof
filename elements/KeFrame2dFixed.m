function [KMembraine, KBending] = KeFrame2dFixed(x1, y1, x2, y2, E, A, I)
    dx = x2-x1;
    dy = y2-y1;
    l = sqrt(dx^2 + dy^2);
    c = dx/l;
    s = dy/l;
    KMembraine = zeros(6);
    KBending = zeros(6);
    KBending([3 6],[3 6]) = 2*E*I/l*[2 1; 1 2];
    KBending([2 5],[2 5]) = 12*E*I/l^3*[1 -1; -1 1];
    KBending(2,3) = 6*E*I/l^2;
    KBending(3,2) = KBending(2,3); KBending(2,6) = KBending(2,3); KBending(6,2) = KBending(2,3);
    KBending(3,5) = -KBending(2,3); KBending(5,3) = -KBending(2,3); KBending(5,6) = -KBending(2,3); KBending(6,5) = -KBending(2,3);
    KMembraine([1 4],[1 4]) = E*A/l*[1 -1; -1 1];
    T = [c s 0  0 0 0; ...
        -s c 0  0 0 0; ...
         0 0 1  0 0 0; ...
         0 0 0  c s 0; ...
         0 0 0 -s c 0; ...
         0 0 0  0 0 1];
    KBending = T'*KBending*T;
    KMembraine = T'*KMembraine*T;
end