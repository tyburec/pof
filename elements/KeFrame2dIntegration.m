function [K,l] = Ke_beamquad_gauss(x1, y1, x2, y2, x3, y3, E1, E2, E3, A1, A2, A3 I1, I2, I3)
    
    %%
    dx = x2-x1; dx2 = x3-x1;
    dy = y2-y1; dy2 = y3-y1;
    l = sqrt(dx2^2 + dy2^2);
    c = dx2/l;
    s = dy2/l;
    if abs(dx/norm(dx)-dx2/norm(dx2)) >= 1e-6
        error();
    end
    
    %% SHAPE FUNCTIONS
    N1 = @(x) 1/2 - x/2;
    N2 = @(x) 1/2 + x/2;
    Bm = @(x) [-1/2 1/2];
    dX = 2/l;
    Bb = @(x) [3*x/l 1.5*x-0.5 -3*x/l 1.5*x+0.5];

%     gp = [-0.5773502691896257 0.5773502691896257];
%     gw = [1 1];
    gp = [-1 0 1];
    gw = [1/3 4/3 1/3];
    
    for i=1:length(gp)
        x = gp(i);
        if (exist('K','var'))
            K([2 3 5 6],[2 3 5 6]) = K([2 3 5 6],[2 3 5 6]) + gw(i) * Bb(x)' * [N1(x) N2(x)]*[E1*I1; E2*I2] * Bb(x) * dX;
        else
            K([2 3 5 6],[2 3 5 6]) = gw(i) * Bb(x)' * [N1(x) N2(x)]*[E1*I1; E2*I2] * Bb(x) * dX;
        end
    end
    
    for i=1:length(gp)
        x = gp(i);
        K([1,4],[1,4]) = K([1,4],[1,4]) + gw(i) * Bm(x)' * [N1(x) N2(x)]*[E1*A1; E2*A2] * Bm(x) * dX;
    end
    
    T = [c s 0  0 0 0; ...
        -s c 0  0 0 0; ...
         0 0 1  0 0 0; ...
         0 0 0  c s 0; ...
         0 0 0 -s c 0; ...
         0 0 0  0 0 1];
    K = T'*K*T;
end