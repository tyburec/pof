function plotRelaxationSpace(fname, nPointsInter, nPointsEdge)
    try 
        run(fname);
    catch
        addpath(genpath('.'));
        run(fname);
    end
    
    if ~exist('nPointsInter','var')
        nPointsInter = 40;
    end
    if ~exist('nPointsEdge', 'var')
        nPointsEdge = 300;
    end
    
    if exist('frameObject','var')
        nel = frameObject.numberOfElements;
    elseif exist('shellObject','var')
        nel = shellObject.numberOfElements;
    else
        error('Can not find number of elements.');
    end
    
    switch nel
    case 1
        plotRelaxationSpace2(fname, nPointsInter);

    case 2
        plotRelaxationSpace3(fname, nPointsInter, nPointsEdge);

    otherwise
        error('plotRelaxationSpace() works only for 1 or 2 elements.');

    end

end