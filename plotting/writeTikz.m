function writeTikz(fname)
    try 
        run(fname);
    catch
        addpath(genpath('.'));
    end

    % load problem definition
    run(fname);

    % nodal labels
    nodeNames = 'a':'z';
    nodeNames = nodeNames(1:frameObject.numberOfNodes);

    fprintf('\\begin{tikzpicture}\n');
    fprintf('\\scaling{5}\n');
    
    % nodes
    for n=1:frameObject.numberOfNodes
        fprintf('\\point{%s}{%f}{%f}\n', nodeNames(n), frameObject.nodes(n,1), frameObject.nodes(n,2));
        fprintf('\\notation{1}{%s}{\\circled{$%d$}}[below=6mm]\n', nodeNames(n), n);
    end

    % elements
    for e=1:frameObject.numberOfElements
        e1 = frameObject.elements(e,1);
        e2 = frameObject.elements(e,2);
        fprintf('\\beam{2}{%s}{%s}\n', nodeNames(e1), nodeNames(e2));
        fprintf('\\notation{4}{%s}{%s}[$%d$]\n', nodeNames(e1), nodeNames(e2),e);
    end

    % supports
    clamped = intersect(intersect(frameObject.idUx{1}, frameObject.idUy{1}), frameObject.idFiz{1});
    fixed   = setdiff(intersect(frameObject.idUx{1}, frameObject.idUy{1}), clamped);
    xrot    = setdiff(intersect(frameObject.idUx{1}, frameObject.idFiz{1}), clamped);
    yrot    = setdiff(intersect(frameObject.idUy{1}, frameObject.idFiz{1}), clamped);
    xdir    = setdiff(frameObject.idUx{1},[fixed clamped xrot]);
    ydir    = setdiff(frameObject.idUy{1},[fixed clamped yrot]);
    rotz    = setdiff(frameObject.idFiz{1},[fixed clamped xrot yrot]);
    for s=1:numel(clamped)
        fprintf('\\support{3}{%s}[270]\n',nodeNames(clamped(s)));
    end
    for s=1:numel(fixed)
        fprintf('\\support{1}{%s}[0]\n',nodeNames(fixed(s)));
    end
    for s=1:numel(xrot)
        fprintf('\\support{4}{%s}[0]\n',nodeNames(xrot(s)));
    end
    for s=1:numel(yrot)
        fprintf('\\support{4}{%s}[90]\n',nodeNames(yrot(s)));
    end
    for s=1:numel(xdir)
        fprintf('\\support{2}{%s}[0]\n',nodeNames(xdir(s)));
    end
    for s=1:numel(ydir)
        fprintf('\\support{2}{%s}[90]\n',nodeNames(ydir(s)));
    end
    if numel(rotz)>0
        error('Not supported');
    end

    % dimensions
    xlow = min(frameObject.nodes(:,1));
    xhig = max(frameObject.nodes(:,1));
    ylow = min(frameObject.nodes(:,2));
    yhig = max(frameObject.nodes(:,2));
    xunq = unique(frameObject.nodes(:,1));
    yunq = unique(frameObject.nodes(:,2));
    for xx=1:numel(xunq)
        fprintf('\\point{d%d}{%f}{%f}\n', xx, xunq(xx), ylow-0.25*(yhig-ylow))
        if xx>1
            fprintf('\\dimensioning{1}{d%d}{d%d}{%f}[$%f$]\n', xx-1, xx, -1*(yhig-ylow), xunq(xx)-xunq(xx-1));
        end
    end
    for yy=1:numel(yunq)
        fprintf('\\point{e%d}{%f}{%f}\n', yy, xhig+0.25*(xhig-xlow), yunq(yy))
        if yy>1
            fprintf('\\dimensioning{2}{e%d}{e%d}{%f}[$%f$]\n', yy-1, yy, -1*(xhig-xlow), yunq(yy)-yunq(yy-1));
        end
    end

    % loads
    for i=1:numel(frameObject.loadIdFx{1})
        fprintf('\\load{1}{%s}[0][1.0][0.0]\n', nodeNames(frameObject.loadIdFx{1}(i)));
        fprintf('\\notation{1}{%s}{$%f$}[above=4mm]\n', nodeNames(frameObject.loadIdFx{1}(i)), frameObject.loadFx{1}(i));
    end
    for i=1:numel(frameObject.loadIdFy{1})
        fprintf('\\load{1}{%s}[90][1.0][0.0]\n', nodeNames(frameObject.loadIdFy{1}(i)));
        fprintf('\\notation{1}{%s}{$%f$}[right=4mm]\n', nodeNames(frameObject.loadIdFy{1}(i)), frameObject.loadFy{1}(i));
    end
    for i=1:numel(frameObject.loadIdMz{1})
        fprintf('\\load{2}{%s}[90][-180][0.5]\n', nodeNames(frameObject.loadIdMz{1}(i)));
        fprintf('\\notation{1}{%s}{$%f$}[right=4mm]\n', nodeNames(frameObject.loadIdMz{1}(i)), frameObject.loadMz{1}(i));
    end
    
    fprintf('\\end{tikzpicture}\n');
end