function plotFrameCircular(nodes, frames, oradius, iradius)

oradius = max(oradius, 0);
oradius(oradius/max(oradius)<=5e-2) = 0;

if ~exist('iradius', 'var')
    iradius = zeros(size(oradius));
end

figure;
hold on;
for i=1:size(frames,1)
    if oradius(i)>0
        coor1 = nodes(frames(i,1),:);
        coor2 = nodes(frames(i,2),:);
        fdeltas = coor2-coor1;
        l = norm(fdeltas);
        fangle = fdeltas/l;
        c = fangle(1); s = fangle(2);

        oVal = oradius(i);
        iVal = iradius(i);

        [Yo,Zo,Xo] = cylinder(oVal);
        [Yi,Zi,Xi] = cylinder(iVal);
        nn = size(Xo,2);
        Xo = Xo'*l; Yo = Yo'; Zo=Zo';
        Xi = Xi'*l; Yi = Yi'; Zi=Zi';
        fonodes = [Xo(:) Yo(:) Zo(:)]';
        finodes = [Xi(:) Yi(:) Zi(:)]';

        R = [c -s 0; s c 0; 0 0 1];
        fonodes = R*fonodes;
        finodes = R*finodes;
        fonodes(1,:) = fonodes(1,:) + coor1(1);
        finodes(1,:) = finodes(1,:) + coor1(1);
        fonodes(2,:) = fonodes(2,:) + coor1(2);
        finodes(2,:) = finodes(2,:) + coor1(2);

        finX = [finodes(1,1:nn); finodes(1,nn+1:end)];
        finY = [finodes(2,1:nn); finodes(2,nn+1:end)];
        finZ = [finodes(3,1:nn); finodes(3,nn+1:end)];
        fonX = [fonodes(1,1:nn); fonodes(1,nn+1:end)];
        fonY = [fonodes(2,1:nn); fonodes(2,nn+1:end)];
        fonZ = [fonodes(3,1:nn); fonodes(3,nn+1:end)];

        surf(finX, finY, finZ, 'FaceColor', [0.8 0.8 0.8]);%, 'EdgeColor', 'none');
        surf(fonX, fonY, fonZ, 'FaceColor', [0.8 0.8 0.8]);%, 'EdgeColor', 'none');
        plot3(finX(1,:), finY(1,:), finZ(1,:),'k');
        plot3(fonX(1,:), fonY(1,:), fonZ(1,:),'k');
        plot3(finX(2,:), finY(2,:), finZ(2,:),'k');
        plot3(fonX(2,:), fonY(2,:), fonZ(2,:),'k');
        for j=1:size(finX,2)-1
            patch([finX(1,j) fonX(1,j) fonX(1,j+1) finX(1, j+1)], ...
                  [finY(1,j) fonY(1,j) fonY(1,j+1) finY(1, j+1)], ...
                  [finZ(1,j) fonZ(1,j) fonZ(1,j+1) finZ(1, j+1)], ...
                  'r', 'FaceColor', [0.8 0.8 0.8], 'EdgeColor', 'none');
            patch([finX(2,j) fonX(2,j) fonX(2,j+1) finX(2, j+1)], ...
                  [finY(2,j) fonY(2,j) fonY(2,j+1) finY(2, j+1)], ...
                  [finZ(2,j) fonZ(2,j) fonZ(2,j+1) finZ(2, j+1)], ...
                  'r', 'FaceColor', [0.8 0.8 0.8], 'EdgeColor', 'none');
        end
    end
end

axis equal off;
ax = gca;
ax.CameraUpVectorMode = 'manual';
ax.CameraUpVector = [0 1 0];
ax.CameraPositionMode = 'manual';
ax.CameraPosition = [3 2 5];
fig=gcf; fig.PaperOrientation='landscape';
fig.PaperPositionMode='auto';
fig.Renderer = 'opengl';
hold off;
end