function plotFrame(frameObject, areas)

switch frameObject.crossSection
    case 'circle'
        [oradius, iradius] = frameObject.getPlottingParam(areas);
        plotFrameCircular(frameObject.nodes, frameObject.elements, oradius, iradius);
        
    case 'square'
        [osize, isize] = frameObject.getPlottingParam(areas);
        plotFrameRectangular(frameObject.nodes, frameObject.elements, osize, osize, isize, isize);
        
    case 'rectangle'
        [owidth, oheight, iwidth, iheight] = frameObject.getPlottingParam(areas);
        plotFrameRectangular(frameObject.nodes, frameObject.elements, owidth, oheight, iwidth, iheight);
        
    otherwise
        error('Unknown cross section type');
end

end