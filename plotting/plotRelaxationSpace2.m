function plotRelaxationSpace2(fname, nPointsInter)

    % load problem definition
    run(fname);

    if frameObject.numberOfElements~=1
        error('plotRelaxationSpace2() function works only for 1 element.');
    end

    opts = sdpsettings('cachesolvers', true);
    opts.verbose = sumOfSquaresObject.verboseSolver;

    % initialize problem data (FEM)
    problem = framesBuildProblem(frameObject);

    % solve moment-sum-of-squares hierarchy to construct lower bounds, upper bounds, and globally optimal solutions
    % keep the moment problems data to allow for reconstruction of the feasible set
    momentProblem = cell(sumOfSquaresObject.maximumRelaxationNumber,1);
    sumOfSquaresObjectCell = cell(sumOfSquaresObject.maximumRelaxationNumber,1);
    lastSumOfSquaresObject = sumOfSquaresObject;
    for relaxation=1:sumOfSquaresObject.maximumRelaxationNumber
        [sumOfSquaresObjectCell{relaxation}, momentProblem{relaxation}] = framesSumOfSquaresHierarchy(problem, lastSumOfSquaresObject, relaxation);
        lastSumOfSquaresObject = sumOfSquaresObjectCell{relaxation};
        maximumRelaxationNumber = relaxation;
        if lastSumOfSquaresObject.isOptimal
            break;
        end
    end

    objectiveUniform = momentProblem{1}.objectiveUniform;

    plotSet = cell(1+maximumRelaxationNumber,1);
    for k=1:maximumRelaxationNumber+1
        plotSet{k} = nan(nPointsInter, 2);
    end
    for i=1:nPointsInter
        a1 = (i-1)/(nPointsInter-1)*problem.volumeBound/problem.elementLengths(1);
        % relaxations
        for k=1:maximumRelaxationNumber
            addConstr = [momentProblem{k}.objective<=objectiveUniform; momentProblem{k}.areas(1)==a1];
            
            diag = optimize([momentProblem{k}.constraintsRelaxed; addConstr], momentProblem{k}.objective, opts);
            if diag.problem==0
                plotSet{k}(i,:) = [a1 value(momentProblem{k}.objective)];
            end
        end
        % true feasible set
        plotSet{end}(i,:) = [a1,computeCompliance(problem,a1)];
    end

    % plotting
    figure; hold on;
    h = cell(maximumRelaxationNumber+1,1);
    for k=1:maximumRelaxationNumber+1
        h{k} = plot(plotSet{k}(:,1), plotSet{k}(:,2));
    end
    hold off;
end