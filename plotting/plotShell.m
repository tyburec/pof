function plotShell(shellObject, thicknesses)
    nodes = shellObject.nodes;
    shells = shellObject.elements;

    thicknesses = max(thicknesses,0);
    
    figure;
    hold on;
    for i=1:size(shells,1)
        coor1 = nodes(shells(i,1),:);
        coor2 = nodes(shells(i,2),:);
        coor3 = nodes(shells(i,3),:);
        coor4 = nodes(shells(i,4),:);

        Xo = [coor1(1) coor4(1) coor4(1) coor1(1) coor1(1); ...
              coor2(1) coor3(1) coor3(1) coor2(1) coor2(1)];
        Yo = [coor1(2) coor4(2) coor4(2) coor1(2) coor1(2); ...
              coor2(2) coor3(2) coor3(2) coor2(2) coor2(2)];
        Zo = [-1/2 -1/2 1/2 1/2 -1/2; ...
              -1/2 -1/2 1/2 1/2 -1/2]*thicknesses(i);
        
        fonX = Xo; fonY = Yo; fonZ = Zo;
        surf(fonX, fonY, fonZ, 'FaceColor', [0.8 0.8 0.8]);%, 'EdgeColor', 'none');
        patch(Xo(1,:),Yo(1,:),Zo(1,:),1,'FaceColor', [0.8 0.8 0.8]);
        patch(Xo(2,:),Yo(2,:),Zo(2,:),1,'FaceColor', [0.8 0.8 0.8]);
        plot3(fonX(1,:), fonY(1,:), fonZ(1,:),'k');
        plot3(fonX(2,:), fonY(2,:), fonZ(2,:),'k');
    end
    
    axis equal tight off;
    ax = gca;
    ax.CameraUpVectorMode = 'manual';
    ax.CameraUpVector = [0 0 1];
    ax.CameraPositionMode = 'manual';
    ax.CameraPosition = [3 -5 2];
    fig=gcf; fig.PaperOrientation='landscape';
    fig.PaperPositionMode='auto';
    fig.Renderer = 'opengl';
    hold off;
    end