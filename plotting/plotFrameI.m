function plotFrame2TP(nodes, frames, areas1, areas2, aligned)

areas1 = max(areas1, 0);
areas1(areas1<=1e-4) = 0;

if ~exist('areas2', 'var')
    areas2 = areas1;
end
areas2(areas2<=1e-4) = 0;

if ~exist('aligned','var')
    aligned = false;
end

figure;
hold on;

for i=1:size(frames,1)
    coor1 = nodes(frames(i,1),:);
    coor2 = nodes(frames(i,2),:);
    fdeltas = coor2-coor1;
    l = norm(fdeltas);
    fangle = fdeltas/l;
    c = fangle(1); s = fangle(2);
    
    bVal = sqrt(areas1(i)/18);
    eVal = sqrt(areas2(i)/18);
    
    Z = [-2.5 2.5 2.5 0.5 0.5 2.5 2.5 -2.5 -2.5 -0.5 -0.5 -2.5 -2.5];
    Z = [Z*bVal; Z*eVal];
    Y = [-5 -5 -4 -4 4 4 5 5 4 4 -4 -4 -5] - (aligned*5);
    Y = [Y*bVal; Y*eVal];
    X = [zeros(1,13); ones(1,13)];
    nn = size(X,2);
    X = X'*l; Y = Y'; Z=Z';
    fnodes = [X(:) Y(:) Z(:)]';
    
%     fnodes = [0 -bVal -bVal; 0 -bVal bVal; 0 bVal -bVal; 0 bVal bVal; ...
%               l -eVal -eVal; l -eVal eVal; l eVal -eVal; l eVal eVal]';
    R = [c -s 0; s c 0; 0 0 1];
    fnodes = R*fnodes;
    fnodes(1,:) = fnodes(1,:) + coor1(1);
    fnodes(2,:) = fnodes(2,:) + coor1(2);
    
    finX = [fnodes(1,1:nn); fnodes(1,nn+1:end)];
    finY = [fnodes(2,1:nn); fnodes(2,nn+1:end)];
    finZ = [fnodes(3,1:nn); fnodes(3,nn+1:end)];
    if (bVal+eVal>1e-3)
        surf(finX, finY, finZ,'FaceColor', [0.8 0.8 0.8]);
        fill3(finX(1,:),finY(1,:),finZ(1,:),'r','FaceColor', [0.8 0.8 0.8])
        fill3(finX(2,:),finY(2,:),finZ(2,:),'r','FaceColor', [0.8 0.8 0.8])
    end
end

axis equal off;
ax = gca;
ax.CameraUpVectorMode = 'manual';
ax.CameraUpVector = [0 1 0];
ax.CameraPositionMode = 'manual';
ax.CameraPosition = [max(nodes(:,1))+2 2 5];
fig=gcf; fig.PaperOrientation='landscape';
fig.PaperPositionMode='auto';
fig.Renderer = 'opengl';
scatter(nodes(:,1), nodes(:,2), 1e-10);
hold off;
end

