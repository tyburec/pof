function plotFrame2SQ(nodes, frames, areas1, areas2)

areas1 = max(areas1, 0);
areas1(areas1<=1e-4) = 0;

if ~exist('areas2', 'var')
    areas2 = areas1;
end
areas2(areas2<=1e-4) = 0;

figure;
hold on;

for i=1:size(frames,1)
    coor1 = nodes(frames(i,1),:);
    coor2 = nodes(frames(i,2),:);
    fdeltas = coor2-coor1;
    l = norm(fdeltas);
    fangle = fdeltas/l;
    c = fangle(1); s = fangle(2);
    bVal = sqrt(areas1(i))/2;
    eVal = sqrt(areas2(i))/2;
    
    fnodes = [0 -bVal -bVal; 0 -bVal bVal; 0 bVal -bVal; 0 bVal bVal; ...
              l -eVal -eVal; l -eVal eVal; l eVal -eVal; l eVal eVal]';
    R = [c -s 0; s c 0; 0 0 1];
    fnodes = R*fnodes;
    fnodes(1,:) = fnodes(1,:) + coor1(1);
    fnodes(2,:) = fnodes(2,:) + coor1(2);
    
    fv.vertices = fnodes';
    fv.faces = [1 2 4 3; 5 6 8 7; 1 5 7 3; 1 5 6 2; 4 8 6 2; 4 8 7 3];
    
    if (bVal+eVal>1e-3)
        patch(fv, 'FaceColor', [0.8 0.8 0.8]);
    end
end

axis equal off;
ax = gca;
ax.CameraUpVectorMode = 'manual';
ax.CameraUpVector = [0 1 0];
ax.CameraPositionMode = 'manual';
ax.CameraPosition = [3 2 5];
fig=gcf; fig.PaperOrientation='landscape';
fig.PaperPositionMode='auto';
fig.Renderer = 'opengl';
hold off;
end

