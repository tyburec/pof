function plotConvergence(fname)

    run(fname);
    if exist('frameObject','var')
        problem = frameObject;
    elseif exist('shellObject','var')
        problem = shellObject;
    else
        error('Can not find problem data');
    end
    [nonlinearSDPObject, fminconObject, optimalityCriteriaObject, mmaObject, momentSumOfSquaresObject] = optimizeStructure(fname);
    
    fig = figure;
    hold on;
    relOrder = sum(~isnan(momentSumOfSquaresObject.objectiveLowerBound));
    xData = 1:relOrder;
    plot(xData, momentSumOfSquaresObject.objectiveLowerBound(1:relOrder), '-k');
    plot(xData, momentSumOfSquaresObject.objectiveUpperBound(1:relOrder), '--k');
%     plot(xData, nonlinearSDPObject.objective*ones(1,relOrder), '-.k');
%     plot(xData, optimalityCriteriaObject.objective*ones(1,relOrder), ':k');
%     plot(xData, fminconObject.objective*ones(1,relOrder), ':r');
    scatter(xData(end), momentSumOfSquaresObject.objective, 80, 'h'); 
%     legend('Moment-SOS lower bounds', 'Moment-SOS upper bounds', 'NSDP upper bound', 'OC upper bound', 'fmincon upper bound', 'Optimal solution');
    legend('Moment-SOS lower bounds', 'Moment-SOS upper bounds', 'Optimal solution', 'Interpreter','latex','Location','southeast');
    xlabel('Relaxation order','Interpreter','latex');
    ylabel('$\omega^{\mathrm{T}} \mathbf{c}$','Interpreter','latex');
    xticks(1:relOrder);
    
    fig.PaperPositionMode = 'manual';
    fig.PaperUnits = 'centimeters';
    fig.PaperSize = [8.4 6];
    set(gca,'TickLabelInterpreter', 'tex');
    
    set(gca,'Units','normalized');
    set(gca,'OuterPosition',[0 0 1 1]);
    
    set(gca,'ActivePositionProperty','outerposition');
    
    for i=1:relOrder
        h = figure;
        plotStructure(problem, momentSumOfSquaresObject.areas{i});
        axH = copyobj(gca, fig);
        set(axH, 'Position',[0.15+(i-1)*0.79/relOrder 0.68-(i-1)*0.45/i 0.79/relOrder 0.3]);
        close(h);
    end
    hold off;
    figure(fig);

    print(['./output/',fname,'_convergencePO.png'],'-dpng','-r300');
    print(['./output/',fname,'_convergencePO.pdf'],'-dpdf','-r300');
    saveas(gcf, ['./output/', fname,'_convergencePO.fig']);
end