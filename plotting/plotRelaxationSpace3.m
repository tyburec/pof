function plotRelaxationSpace3(fname, nPointsInter, nPointsEdge)
    % display options
    customUB = nan;   % to show sublevelset c<=customUB
    displayUniformUB = true;
    displayTrussUB = true;

    % load problem definition
    run(fname);

    % initialize problem data (FEM)
    if exist('frameObject','var')
        problem = framesBuildProblem(frameObject);
    elseif exist('shellObject','var')
        problem = shellsBuildProblem(shellObject);
    else
        error('Unknown object type');
    end
    
    if problem.numberOfElements~=2
        error('plotRelaxationSpace3() function works only for 2 elements.');
    end

    opts = sdpsettings('cachesolvers', true);
    if ~exist('momentSumOfSquaresObject','var')
        momentSumOfSquaresObject = defaultMomentSumOfSquaresObject(problem.numberOfElements, problem.numberOfLoadCases);
    end
    opts.verbose = momentSumOfSquaresObject.verboseSolver;
    
    momentSumOfSquaresObject.customComplianceUpperBound = customUB;
    
    % solve moment-sum-of-squares hierarchy to construct lower bounds, upper bounds, and globally optimal solutions
    % keep the moment problems data to allow for reconstruction of the feasible set
    momentProblem = cell(momentSumOfSquaresObject.maximumRelaxationNumber,1);
    sumOfSquaresObjectCell = cell(momentSumOfSquaresObject.maximumRelaxationNumber,1);
    lastSumOfSquaresObject = momentSumOfSquaresObject;
    lowestRelaxationNumber = 1+(problem.factor3>0)*1;
    for relaxation=lowestRelaxationNumber:momentSumOfSquaresObject.maximumRelaxationNumber
        [sumOfSquaresObjectCell{relaxation}, momentProblem{relaxation}] = framesSumOfSquaresHierarchy(problem, lastSumOfSquaresObject, relaxation);
        lastSumOfSquaresObject = sumOfSquaresObjectCell{relaxation};
        maximumRelaxationNumber = relaxation;
        if lastSumOfSquaresObject.isOptimal
            break;
        end
    end
    
    % truss upper bound
    [objectiveTruss, aTruss] = framesTrussSDPUpperBound(problem);
    objectiveUniform = momentProblem{lowestRelaxationNumber}.objectiveUniform;
    if ~isnan(customUB)
        ubObjective = customUB;
    else
        ubObjective = min((~isinf(objectiveTruss))*objectiveTruss, (~isinf(objectiveUniform))*objectiveUniform);
    end

    fprintf('plotRelaxationSpace3(): computing points along the volume boundary...');
    tic
    % points along volume boundary (\ell'a==V)
    volBoundary = cell(1+maximumRelaxationNumber,1);
    for k=lowestRelaxationNumber:maximumRelaxationNumber+1
        volBoundary{k} = nan(nPointsEdge, 3);
    end
    for i=1:nPointsEdge
        a1 = (i-1)/(nPointsEdge-1)*problem.volumeBound/problem.elementLengths(1);
        a2 = (problem.volumeBound-a1*problem.elementLengths(1))/problem.elementLengths(2);
        % relaxations
        for k=lowestRelaxationNumber:maximumRelaxationNumber
            if a1==0
                nObj = -momentProblem{k}.areas(2);
                addConstr = momentProblem{k}.objective<=ubObjective;
            elseif a2==0
                nObj = -momentProblem{k}.areas(1);
            else
                nObj = -problem.elementLengths'*momentProblem{k}.areas;
                addConstr = [momentProblem{k}.objective<=ubObjective; a1*momentProblem{k}.areas(2)-a2*momentProblem{k}.areas(1)==0];
            end
            
            diag = optimize([momentProblem{k}.constraintsRelaxed; addConstr], nObj, opts);
            if diag.problem==0
                if (problem.elementLengths'*value(momentProblem{k}.areas)>=problem.volumeBound-1e-5)
                    addConstr = [momentProblem{k}.areas(1)==a1; momentProblem{k}.areas(2)==a2];
                    diag = optimize([momentProblem{k}.constraintsRelaxed; addConstr], momentProblem{k}.objective, opts);
                    if diag.problem==0
                        volBoundary{k}(i,:) = [a1,a2,value(momentProblem{k}.objective)];
                    end
                else
                    volBoundary{k}(i,:) = [a1,a2,ubObjective];
                end
            end
        end
        % true feasible set -- bisection TODO
        volBoundary{end}(i,:) = [a1,a2,computeCompliance(problem,[a1;a2])];
    end
    fprintf('%f s.\n', toc)

    % points at a1, compliance<=uniform boundary
    fprintf('plotRelaxationSpace3(): computing points at a1 boundary, cuniform...');
    tic
    a1Boundary = cell(1+maximumRelaxationNumber,1);
    for k=lowestRelaxationNumber:maximumRelaxationNumber+1
        a1Boundary{k} = nan(nPointsEdge, 3);
    end
    for i=1:nPointsEdge
        a1 = (i-1)/(nPointsEdge-1)*problem.volumeBound/problem.elementLengths(1);

        % bisection to compute a2
        a2u = problem.volumeBound/problem.elementLengths(2);
        c2b = computeCompliance(problem,[a1;a2u]);
        a2b = 0.0;
        c2u = computeCompliance(problem,[a1;a2b]);
        if c2b>ubObjective
            continue;
        elseif c2u<ubObjective
            a1Boundary{end}(i,:) = [a1 a2b c2u];
        else
            while true
                a2mid = (a2u + a2b)/2;
                c2mid = computeCompliance(problem,[a1;a2mid]);
                if c2mid<ubObjective
                    a2u = a2mid;
                    c2b = c2mid;
                else
                    a2b = a2mid;
                    c2u = c2mid;
                end
                if abs(c2u-c2b)<1e-6
                    break;
                end
            end
            if (problem.elementLengths'*[a1; mean([a2u,a2b])])<=problem.volumeBound
                a1Boundary{end}(i,:) = [a1, mean([a2u,a2b]), mean([c2b,c2u])];
            end
        end

        % relaxations
        for k=lowestRelaxationNumber:maximumRelaxationNumber
            addConstr = [momentProblem{k}.areas(1)==a1; momentProblem{k}.objective<=ubObjective];
            diag = optimize([momentProblem{k}.constraintsRelaxed; addConstr], momentProblem{k}.areas(2), opts);
            if diag.problem==0
                addConstr = [momentProblem{k}.areas(1)==a1; momentProblem{k}.areas(2)==value(momentProblem{k}.areas(2))];
                diag = optimize([momentProblem{k}.constraintsRelaxed; addConstr], momentProblem{k}.objective, opts);
                if diag.problem==0
                    a1Boundary{k}(i,:) = [a1,value(momentProblem{k}.areas(2)),value(momentProblem{k}.objective)];
                end
            end
        end
    end
    fprintf('%f s.\n', toc);

    % points at a2=0, compliance<=uniform boundary
    fprintf('plotRelaxationSpace3(): computing points at a2 boundary, cuniform...');
    tic
    a2Boundary = cell(1+maximumRelaxationNumber,1);
    for k=lowestRelaxationNumber:maximumRelaxationNumber+1
        a2Boundary{k} = nan(nPointsEdge, 3);
    end
    for i=1:nPointsEdge
        a2 = (i-1)/(nPointsEdge-1)*problem.volumeBound/problem.elementLengths(2);

        % bisection to compute a1
        a1u = problem.volumeBound/problem.elementLengths(1);
        c1b = computeCompliance(problem,[a1u;a2]);
        a1b = 0.0;
        c1u = computeCompliance(problem,[a1b;a2]);
        if c1b>ubObjective
            continue;
        elseif c1u<ubObjective
            a2Boundary{end}(i,:) = [a1b a2 c1u];
        else
            while true
                a1mid = (a1u + a1b)/2;
                c1mid = computeCompliance(problem,[a1mid;a2]);
                if c1mid<ubObjective
                    a1u = a1mid;
                    c1b = c1mid;
                else
                    a1b = a1mid;
                    c1u = c1mid;
                end
                if abs(c1u-c1b)<1e-6
                    break;
                end
            end
            if (problem.elementLengths'*[mean([a1b,a1u]); a2])<=problem.volumeBound
                a2Boundary{end}(i,:) = [mean([a1b,a1u]), a2, mean([c1b,c1u])];
            end
        end

        % relaxations
        for k=lowestRelaxationNumber:maximumRelaxationNumber
            addConstr = [momentProblem{k}.areas(2)==a2; momentProblem{k}.objective<=ubObjective];
            diag = optimize([momentProblem{k}.constraintsRelaxed; addConstr], momentProblem{k}.areas(1), opts);
            if diag.problem==0
                addConstr = [momentProblem{k}.areas(1)==value(momentProblem{k}.areas(1)); momentProblem{k}.areas(2)==a2];
                diag = optimize([momentProblem{k}.constraintsRelaxed; addConstr], momentProblem{k}.objective, opts);
                if diag.problem==0
                    a2Boundary{k}(i,:) = [value(momentProblem{k}.areas(1)),a2,value(momentProblem{k}.objective)];
                end
            end
        end
    end
    fprintf('%f s.\n', toc);

    % interior points of the feasible set (TODO - sample points in triangle)
    fprintf('plotRelaxationSpace3(): computing interior points...');
    tic
    interiorPoints = cell(1+maximumRelaxationNumber,1);
    for k=lowestRelaxationNumber:maximumRelaxationNumber+1
        interiorPoints{k} = nan(nPointsInter^2, 3);
    end
    for i=1:nPointsInter
        for j=1:nPointsInter
            a1 = (i-1)/(nPointsInter-1)*problem.volumeBound/problem.elementLengths(1);
            a2 = (j-1)/(nPointsInter-1)*problem.volumeBound/problem.elementLengths(1);
            if (problem.elementLengths'*[a1;a2]>problem.volumeBound)
                continue;
            end
            % relaxations
            for k=lowestRelaxationNumber:maximumRelaxationNumber
                addConstr = [momentProblem{k}.areas(1)==a1; momentProblem{k}.areas(2)==a2];
                diag = optimize([momentProblem{k}.constraintsRelaxed; addConstr], momentProblem{k}.objectiveRelaxed, opts);
                if diag.problem==0
                    interiorPoints{k}(j+(i-1)*nPointsInter,:) = [a1,a2,value(momentProblem{k}.objective)];
                end
            end
            % true feasible set
            interiorPoints{end}(j+(i-1)*nPointsInter,:) = [a1,a2,computeCompliance(problem,[a1;a2])];
        end
    end
    fprintf('%f s.\n', toc);

    % mesh points
    allPoints = cell(maximumRelaxationNumber+1,1);
    triObject = cell(maximumRelaxationNumber+1,1);
    for k=lowestRelaxationNumber:maximumRelaxationNumber+1
        allPoints{k} = [volBoundary{k}; a1Boundary{k}; a2Boundary{k}; interiorPoints{k}];
        if k==maximumRelaxationNumber+1
            if isfield(sumOfSquaresObjectCell{end}, 'solution')
                allPoints{end} = [allPoints{end}; sumOfSquaresObjectCell{end}.solution'];
            end
        else
            allPoints{k} = [allPoints{k}; [sumOfSquaresObjectCell{k}.areas{k}' sumOfSquaresObjectCell{k}.objectiveLowerBound(k)]];
        end
        allPoints{k}(isnan(allPoints{k}(:,3)),:) = [];
        allPoints{k}(allPoints{k}(:,3)>ubObjective,:) = [];
        allPoints{k} = uniquetol(allPoints{k},'ByRows', true);
        if ~isempty(allPoints{k})
            allPoints{k}(ismembertol(allPoints{k},[0,0,0],'ByRows',true),:) = [];
            allPoints{k}(allPoints{k}(:,3)<0,:) = [];
        end
        if k<=maximumRelaxationNumber
            triObject{k} = MyCrustOpen(allPoints{k});
            if numel(unique(triObject{k}))<size(allPoints{k},1)*0.4
                al = alphaShape(allPoints{k}(:,1), allPoints{k}(:,2), 0.1);
                triObject{k} = alphaTriangulation(al);
            end
        else
            triAlpha = alphaShape(allPoints{k}(:,1), allPoints{k}(:,2), 0.1);
            triObject{k} = alphaTriangulation(triAlpha);
        end
    end

    % plotting
    for k=lowestRelaxationNumber:maximumRelaxationNumber+1
        figure(k);
        hold on;
        % meshed points
        ho = trisurf(triObject{k}, allPoints{k}(:,1), allPoints{k}(:,2), allPoints{k}(:,3), 'EdgeColor', 'k');
        % feasible set
        if k<=maximumRelaxationNumber
            ho.FaceAlpha = 0.5;
            fs = trisurf(triObject{end}, allPoints{end}(:,1), allPoints{end}(:,2), allPoints{end}(:,3), 'EdgeColor', 'k');
            fs.FaceAlpha = 1.0;
        end
            
        shading interp;
        % contours
        if size(allPoints{k},1)==2
            plot3(allPoints{k}(:,1), allPoints{k}(:,2), allPoints{k}(:,3), 'k');
        elseif size(allPoints{k},1)>2
            if ~exist('nv','var')
                [~,~,~,~,nv,CS] = tricont(allPoints{k}(:,1), allPoints{k}(:,2), triObject{k}, allPoints{k}(:,3));
                nvR = nv(end)-nv(end-1);
                nv = [nv nv(end)+nvR:nvR:ubObjective];
            else
                [~,~,~,~,~,CS] = tricont(allPoints{k}(:,1), allPoints{k}(:,2), triObject{k}, allPoints{k}(:,3), nv);
            end
            CSz = nan(1,size(CS,2));
            nanPos = [find(isnan(CS(2,:))) size(CS,2)+1];
            for gg=1:numel(nanPos)-1
                CSz(nanPos(gg):nanPos(gg+1)-1) = CS(1,nanPos(gg));
            end            
            plot3(CS(1,:), CS(2,:), CSz, '--k', 'LineWidth', 0.5);
            plot3(CS(1,:), CS(2,:), ones(size(CSz))*ubObjective-1.4*(ubObjective-min(allPoints{lowestRelaxationNumber}(:,3))), '--k', 'LineWidth', 0.5);
        end
        if k<=maximumRelaxationNumber
            if size(allPoints{end},1)==2
                plot3(allPoints{end}(:,1), allPoints{end}(:,2), allPoints{end}(:,3), 'k');
            elseif size(allPoints{end},1)>2
                [~,~,~,~,~,CS] = tricont(allPoints{end}(:,1), allPoints{end}(:,2), triObject{end}, allPoints{end}(:,3), nv);
                CSz = nan(1,size(CS,2));
                nanPos = [find(isnan(CS(2,:))) size(CS,2)+1];
                for gg=1:numel(nanPos)-1
                    CSz(nanPos(gg):nanPos(gg+1)-1) = CS(1,nanPos(gg));
                end            
                plot3(CS(1,:), CS(2,:), CSz, '--k', 'LineWidth', 0.5);
%                 plot3(CS(1,:), CS(2,:), ones(size(CSz))*ubObjective-1.4*(ubObjective-min(allPoints{lowestRelaxationNumber}(:,3))), '--k', 'LineWidth', 0.5);
            end
        end
        % boundary facets
        if k<=maximumRelaxationNumber
            vBound = volBoundary{k}; vBound(isnan(vBound(:,3)),:) = [];
            a1Bound = a1Boundary{k}; a1Bound(isnan(a1Bound(:,3)),:) = [];
            a2Bound = a2Boundary{k}; a2Bound(isnan(a2Bound(:,3)),:) = [];
            plot3(vBound(:,1),vBound(:,2),vBound(:,3),'k');
            plot3(vBound(:,1),vBound(:,2),ones(size(vBound(:,3)))*ubObjective-1.4*(ubObjective-min(allPoints{lowestRelaxationNumber}(:,3))),'k','LineWidth', 0.5);%%%%
            plot3(a1Bound(:,1),a1Bound(:,2),a1Bound(:,3),'k');
            plot3(a1Bound(:,1),a1Bound(:,2),ones(size(a1Bound(:,3)))*ubObjective-1.4*(ubObjective-min(allPoints{lowestRelaxationNumber}(:,3))),'k','LineWidth', 0.5);
            plot3(a2Bound(:,1),a2Bound(:,2),a2Bound(:,3),'k');
            plot3(a2Bound(:,1),a2Bound(:,2),ones(size(a2Bound(:,3)))*ubObjective-1.4*(ubObjective-min(allPoints{lowestRelaxationNumber}(:,3))),'k','LineWidth', 0.5);
            fv1.vertices = [vBound; [vBound(:,[1 2]) ones(size(vBound,1),1)*ubObjective]];
            fv1.faces = [1:size(vBound,1) size(vBound,1)+(size(vBound,1):-1:1)];
            hf1 = patch(fv1); hf1.FaceAlpha = 0.2; hf1.LineStyle='none';
            fv2.vertices = [a1Bound; [a1Bound(:,[1 2]) ones(size(a1Bound,1),1)*ubObjective]];
            fv2.faces = [1:size(a1Bound,1) size(a1Bound,1)+(size(a1Bound,1):-1:1)];
            hf2 = patch(fv2); hf2.FaceAlpha = 0.2; hf2.LineStyle='none';
            fv3.vertices = [a2Bound; [a2Bound(:,[1 2]) ones(size(a2Bound,1),1)*ubObjective]];
            fv3.faces = [1:size(a2Bound,1) size(a2Bound,1)+(size(a2Bound,1):-1:1)];
            hf3 = patch(fv3); hf3.FaceAlpha = 0.2; hf3.LineStyle='none';
            % top faces
            trisurf(triObject{k}, allPoints{k}(:,1), allPoints{k}(:,2), ones(size(allPoints{k},1),1)*ubObjective, 'EdgeColor', 'none', 'FaceAlpha', 0.1);
        end
        % feasible set
        % boundary facets
        bf = boundaryFacets(triAlpha);
        trisurf(bf, allPoints{end}(:,1), allPoints{end}(:,2), allPoints{end}(:,3));
        if k>maximumRelaxationNumber
            trisurf(bf, allPoints{end}(:,1), allPoints{end}(:,2), ones(size(allPoints{end}(:,3)))*ubObjective-1.4*(ubObjective-min(allPoints{lowestRelaxationNumber}(:,3))),'EdgeColor','k','LineWidth', 0.5);
            % vertical faces
            fv.vertices = [allPoints{k}; [allPoints{k}(:,[1 2]) ones(size(allPoints{k},1),1)*ubObjective]];
            fv.faces = [bf fliplr(bf)+size(allPoints{k},1)];
            hf = patch(fv);
            hf.FaceAlpha = 0.1; hf.LineStyle='none';
        end
        
        legloc = 'southoutside';
        legHandles = [];
        legLabels = {};
        % uniform design
        if displayUniformUB
            huni = scatter3(problem.volumeBound/sum(problem.elementLengths), problem.volumeBound/sum(problem.elementLengths), objectiveUniform, 70, 'filled', 'r', 'MarkerEdgeColor', 'k');
            legHandles = [legHandles; huni];
            legLabels = {legLabels{:}, 'Uniform design'};
        end
        if displayTrussUB
            htruss = scatter3(aTruss(1), aTruss(2), objectiveTruss, 70, 'filled', 'w', 'MarkerEdgeColor', 'k');
            legHandles = [legHandles; htruss];
            legLabels = {legLabels{:}, 'Truss UB'};
        end
        % first-order moments
        if k==maximumRelaxationNumber+1
            % local mimimum/a
            isLocalMin = false(size(allPoints{k},1),1);
            pointsCell = cell(size(allPoints{k},1),1);
            for i=1:size(triObject{k},1)
                v1=triObject{k}(i,1); v2=triObject{k}(i,2); v3=triObject{k}(i,3);
                pointsCell{v1} = [pointsCell{v1}, v2, v3];
                pointsCell{v2} = [pointsCell{v2}, v1, v3];
                pointsCell{v3} = [pointsCell{v3}, v1, v2];
            end
            doublePointsCell = cell(size(allPoints{k},1),1);
            for i=1:size(allPoints{k},1)
                doublePointsCell{i} = unique([pointsCell{pointsCell{i}}]);
            end
            for i=1:size(doublePointsCell,1)
                if numel(doublePointsCell{i})>0
                    neighborMin = min(allPoints{k}(doublePointsCell{i},3));
                    if neighborMin>=allPoints{k}(i,3)
                        isLocalMin(i)=true;
                    end
                end
            end
            % global minimum/a
            [~,gMin] = min(allPoints{k}(isLocalMin,3));
            gMinPos = find(isLocalMin); gMinPos = gMinPos(gMin);
            gMin = find((allPoints{k}(:,3)<=allPoints{k}(gMinPos,3)+1e-7) & isLocalMin);
            hglob = scatter3(allPoints{k}(gMin,1), allPoints{k}(gMin,2), allPoints{k}(gMin,3), 70, 'filled', 'yp', 'MarkerEdgeColor', 'k');
            % local minimum
            isLocalMin(gMin) = false;
            if any(isLocalMin)
                hloc = scatter3(allPoints{k}(isLocalMin,1), allPoints{k}(isLocalMin,2), allPoints{k}(isLocalMin,3), 50, 'filled', 'g', 'MarkerEdgeColor', 'k');
                legHandles = [legHandles; hglob; hloc];
                legLabels = {legLabels{:} , 'Global minimum', 'Local minimum'};
            else
                legHandles = [legHandles; hglob];
                legLabels = {legLabels{:}, 'Global minimum'};
            end
        else
            hrel = scatter3(sumOfSquaresObjectCell{k}.areas{k}(1), sumOfSquaresObjectCell{k}.areas{k}(2), sumOfSquaresObjectCell{k}.objectiveLowerBound(k), 50, 'filled', 'y', 'MarkerEdgeColor', 'k');
            hub = scatter3(sumOfSquaresObjectCell{k}.areas{k}(1), sumOfSquaresObjectCell{k}.areas{k}(2), sumOfSquaresObjectCell{k}.objectiveUpperBound(k), 30, 'filled', 'c', 'MarkerEdgeColor', 'k');
            legHandles = [legHandles; hrel; hub];
            legLabels = {legLabels{:}, 'Lower bound','Upper bound'};
        end
        legend(legHandles, legLabels{:},'Interpreter','latex','Location',legloc);
        hold off;
        % plot appearance
        xlim([0 problem.volumeBound/problem.elementLengths(1)]);
        ylim([0 problem.volumeBound/problem.elementLengths(2)]);
        zlim([ubObjective-1.4*(ubObjective-min(allPoints{lowestRelaxationNumber}(:,3))) ubObjective]);
        xlabel('$a_1$','Interpreter','latex');
        ylabel('$a_2$','Interpreter','latex');
        if problem.numberOfLoadCases==1
            zlabel('$c$','Interpreter','latex');
        else
            zlabel('$\omega^\mathrm{T} \mathbf{c}$','Interpreter','latex');
        end
        set(gca,'TickLabelInterpreter', 'latex');
        box on;
%         set(gca,'DataAspectRatio',[problem.elementLengths(1)/problem.volumeBound problem.elementLengths(2)/problem.volumeBound 1.2/(ubObjective-min(allPoints{lowestRelaxationNumber}(:,3)))]);
        view(135,20);
        set(gcf, 'PaperUnits', 'centimeters', 'PaperPosition', [0 0 6 7], 'PaperSize', [6 7]);
        if k<=maximumRelaxationNumber
            ffname = [fname,'_relaxation',num2str(k)];
        else
            ffname = [fname,'_feasibleset'];
        end
        print(['./output/',ffname,'.png'],'-dpng','-r300');
        print(['./output/',ffname,'.pdf'],'-dpdf','-r300');
        saveas(gcf, ['./output/', ffname,'.fig']);
    end
end