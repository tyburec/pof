function plotStructure(problem, variables)
    if size(problem.elements,2)==2
        plotFrame(problem, variables);
    elseif size(problem.elements,2)==4
        plotShell(problem, variables);
    else
        error('ERROR in plotStructure(): Unknown structure type');
    end
end